package main

import (
	"encoding/xml"
	"html/template"
	"io/ioutil"
	"log"
	"net/http"
	"strings"
)

type Data struct {
	Content string `xml:"channel>item>encoded"`
}

type Coin struct {
	Name  string
	Value string
	Image string
}

func removeItem(s []Coin, index int) []Coin {
	return append(s[:index], s[index+1:]...)
}

func formatData(data string) []Coin {
	var coins []Coin

	data0 := strings.TrimLeft(data, "<p>")
	data1 := strings.ReplaceAll(data0, "</p><p></p><p>", "</br>")
	data2 := strings.ReplaceAll(data1, "</br>", "<br/>")
	data3 := strings.Split(data2, "@")
	data4 := strings.Split(data3[0], "<br/>")

	for _, num := range data4 {
		v := strings.Split(num, " ")
		if len(v) > 4 {
			var c = Coin{}
			c.Name = v[1]
			c.Value = v[len(v)-1]
			if len(v) > 6 {
				c.Name = strings.Join(v[1:3], " ")
			}
			if strings.HasPrefix(v[1], "grama") {
				c.Name = strings.Join(v[1:4], " ")
			}
			coins = append(coins, c)
		}
	}
	coins = removeItem(coins, 7) // real em ienes
	coins = removeItem(coins, 11) // real em chilenos
	return coins
}

func dolarhjHandler(w http.ResponseWriter, r *http.Request) {
	var d Data

	resp, err := http.Get("https://dolarhoje.com/feed.xml")
	if err != nil {
		log.Fatal(err)
	}
	bytes, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		log.Fatal(err)
	}
	xml.Unmarshal(bytes, &d)
	defer resp.Body.Close()

	coins := formatData(d.Content)

	t, err := template.ParseFiles("./templates/all-coins.html")
	if err != nil {
		log.Fatal(err)
	}
	t.Execute(w, coins)
}

func main() {
	fs := http.FileServer(http.Dir("./static"))
	http.Handle("/static/", http.StripPrefix("/static/", fs))

	http.HandleFunc("/", dolarhjHandler)
	http.ListenAndServe(":8000", nil)
}
